package com.tolyasdev;

import javax.swing.*;

/**
 * Created by tolyas on 6/22/16.
 */
public class PasswordForm extends JFrame {
    private JTextField txtPassword;
    private JButton btnPasswordOk;
    private JPanel rootPanel;
    private ActionListener actionListener;

    PasswordForm(ActionListener actionListener){
        super("OTP Secure Cipher");
        this.actionListener = actionListener;
        setContentPane(rootPanel);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        btnPasswordOk.addActionListener(e -> {
            dispose();
            actionListener.onActionClick(txtPassword.getText());
        });
        setVisible(true);
    }

    interface ActionListener{
        void onActionClick(String plainText);
    }
}
